package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controller.BunteRechteckeController;

@SuppressWarnings("serial")
public class BunteGUI extends JFrame {
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItemNeuesRechteck;
	private BunteRechteckeController brc;;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		new BunteGUI().run();
	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}
	}

	/**
	 * Create the frame.
	 */
	public BunteGUI() {
		this.brc = new BunteRechteckeController();
		setTitle("RechteckViewTest");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1260, 1080);
		this.menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		this.menu = new JMenu("Neu erstellen");
		this.menuBar.add(menu);
		this.menuItemNeuesRechteck = new JMenuItem("Rechteck neu erstellen");
		this.menuItemNeuesRechteck.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				menuNewRechteck_Clicked();
			}
		}
		);
		this.menu.add(this.menuItemNeuesRechteck);
		contentPane = new Zeichenflaeche(brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setVisible(true);
	}
	public void menuNewRechteck_Clicked() {
		new RechteckLayout(brc);
	}
}