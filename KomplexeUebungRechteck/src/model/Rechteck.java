package model;

import java.util.Random;

public class Rechteck {
	//Attribute
	
	private int x;
	private int y;
	private int breite;
	private int hoehe;
	private Punkt p;
	private static Random rand = new Random();
	public static final int MaxBreite = 1200;
	public static final int MaxHoehe = 1000;
	
	
	//Konstruktoren
	
	public Rechteck() {
		this.p = new Punkt();
		this.breite = breite;
		this.hoehe = hoehe;
		
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		this.p = new Punkt(x,y);
		this.setBreite(breite);
        this.setHoehe(hoehe);
		
		
	}
	public int getX() {
		return this.p.getX();
	}
	public void setX(int x) {
		this.p.setX(x);
	}
	public int getY() {
		return this.p.getY();
	}
	public void setY(int y) {
		this.p.setY(y);
	}
	public int getBreite() {
		return breite;
	}
	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}
	public int getHoehe() {
		return hoehe;
	}
	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	
	public boolean enthaelt(int x, int y) {
		return this.p.getX() <= x && x <= this.p.getX() + this.breite &&
			   this.p.getY() <= y && y <= this.p.getY() + this.hoehe;
	}
	
	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		Punkt linksoben = new Punkt(rechteck.getX(), rechteck.getY());
		Punkt rechtsunten = new Punkt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
		return enthaelt(linksoben) && enthaelt(rechtsunten);
	}

	public static Rechteck generiereZufallsRechteck(){
		int x = rand.nextInt(MaxBreite);
		int y = rand.nextInt(MaxHoehe);
		int breite = rand.nextInt(MaxBreite-x);
		int hoehe = rand.nextInt(MaxHoehe-y);
		return new Rechteck(x, y, breite, hoehe);
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + ", p=" + p + "]";
	}

}
